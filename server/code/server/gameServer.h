#ifndef HG_GAME_SERVER_H
#define HG_GAME_SERVER_H

#include "server.h"
#include "network/packet.h"
#include "game/state.h"

#include <console/console.h>
#include <s3d/util.h>

class ClientID {
public:
    sf::IpAddress mAddress;
    unsigned short mPort;
};
class ClientData {
public:
    ClientID mID;
    sf::Clock mClock;

    game::Player* mPlayer;
};

class GameServer : public Server {
public:
    GameServer();
    ~GameServer();

    void disconnect(net::ServerPacket::Disconnect data = net::ServerPacket::Disconnect());

    const bool hasPlayer(ClientID& id);
    game::Player* getPlayer(ClientID& id);

    void setConsole(Console* console);
    Console* getConsole();
    const bool hasConsole();

    ClientID& getSvClient();
    game::State* state();
private:
    // inherited
    void updateWorld(const float dts);
    void sendWorld();
    void handlePacket(sf::Packet& foreignPacket, sf::IpAddress& foreignAddress, const unsigned short foreignPort);

    // client stuff
    void sendAll(net::ServerPacket& svPacket);
    void sendClient(ClientID& id, net::ServerPacket& svPacket);

    ClientData* getClient(ClientID& id);
    const bool clientExists(ClientID& id);
    ClientData* addClient(ClientID& id, const std::string& name);
    void removeAll();
    void removeClient(ClientID& id);

    void setClientName(ClientID& id, const std::string& name);
    void executeClientCommand(ClientID& id, const std::string& command);

    const std::string getClientInfo(ClientID& id);

    std::vector<ClientData> mClients;
    ClientID mCommandClient;
    Console* mConsole;

    // State
    game::State mState;
};

#endif // HG_GAME_SERVER_H

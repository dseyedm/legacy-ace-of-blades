#include "server/gameServer.h"
#include <cstdlib>
#include <conio.h>
GameServer gServer;
void bind(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const unsigned short port = Console::Variable::convertFromString<unsigned short>(tokens.at(0));
        const unsigned short lastPort = gServer.getPort();
        if(gServer.bindToPort(port)) {
            console->print("successfully bind to port " + Console::Variable::convertToString(port));
        } else {
            console->print("failed to bind to port " + Console::Variable::convertToString(port));
            gServer.bindToPort(lastPort);
        }
    } else {
        console->print("currently bound to " + Console::Variable::convertToString(gServer.getPort()));
    }
}
void setTickrate(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float tickrate = Console::Variable::convertFromString<float>(tokens.at(0));
        gServer.setTickrate(tickrate);
        console->print("successfully set tickrate to " + Console::Variable::convertToString(gServer.getTickrate()));
    } else {
        console->print("tickrate set to " + Console::Variable::convertToString(gServer.getTickrate()));
    }
}
void setTimeout(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float timeout = Console::Variable::convertFromString<float>(tokens.at(0));
        gServer.setTimeout(timeout);
        console->print("successfully set timeout to " + Console::Variable::convertToString(gServer.getTimeout()));
    } else {
        console->print("timeout set to " + Console::Variable::convertToString(gServer.getTimeout()));
    }
}
void setGravity(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 3) {
        glm::vec3 varGravity(Console::Variable::convertFromString<float>(tokens.at(0)),
                             Console::Variable::convertFromString<float>(tokens.at(1)),
                             Console::Variable::convertFromString<float>(tokens.at(2)));
        gServer.state()->setGravity(varGravity);
    } else {
        console->print("gravity set to " +  Console::Variable::convertToString(gServer.state()->getGravity().x) + " " +
                                            Console::Variable::convertToString(gServer.state()->getGravity().y) + " " +
                                            Console::Variable::convertToString(gServer.state()->getGravity().z));
    }
}
void setFriction(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        gServer.state()->setFriction(Console::Variable::convertFromString<float>(tokens.at(0)));
    } else {
        console->print("friction set to " + Console::Variable::convertToString(gServer.state()->getFriction()));
    }
}
void setAirFriction(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        gServer.state()->setAirFriction(Console::Variable::convertFromString<float>(tokens.at(0)));
    } else {
        console->print("air friction set to " + Console::Variable::convertToString(gServer.state()->getAirFriction()));
    }
}
void setRestitution(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        gServer.state()->setRestitution(Console::Variable::convertFromString<float>(tokens.at(0)));
    } else {
        console->print("restitution set to " + Console::Variable::convertToString(gServer.state()->getRestitution()));
    }
}
void setPosition(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 3) {
        glm::vec3 value(Console::Variable::convertFromString<float>(tokens.at(0)),
                        Console::Variable::convertFromString<float>(tokens.at(1)),
                        Console::Variable::convertFromString<float>(tokens.at(2)));
        if(gServer.hasPlayer(gServer.getSvClient())) {
            gServer.getPlayer(gServer.getSvClient())->transform()->setPosition(value);
        }
    } else {
        if(gServer.hasPlayer(gServer.getSvClient())) {
            console->print(Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->transform()->getPosition().x) + " " +
                           Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->transform()->getPosition().y) + " " +
                           Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->transform()->getPosition().z));
        } else {
            console->print("you are not a player");
        }
    }
}
void setVelocity(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 3) {
        glm::vec3 value(Console::Variable::convertFromString<float>(tokens.at(0)),
                        Console::Variable::convertFromString<float>(tokens.at(1)),
                        Console::Variable::convertFromString<float>(tokens.at(2)));
        if(gServer.hasPlayer(gServer.getSvClient())) {
            gServer.getPlayer(gServer.getSvClient())->setVelocity(value);
        }
    } else {
        if(gServer.hasPlayer(gServer.getSvClient())) {
            console->print(Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->getVelocity().x) + " " +
                           Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->getVelocity().y) + " " +
                           Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->getVelocity().z));
        } else {
            console->print("you are not a player");
        }
    }
}
void setMaxVelocity(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float value = Console::Variable::convertFromString<float>(tokens.at(0));
        if(gServer.hasPlayer(gServer.getSvClient())) {
            gServer.getPlayer(gServer.getSvClient())->setMaxVelocity(value);
        }
    } else {
        if(gServer.hasPlayer(gServer.getSvClient())) {
            console->print(Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->getMaxVelocity()));
        } else {
            console->print("you are not a player");
        }
    }
}
void setAcceleration(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float value = Console::Variable::convertFromString<float>(tokens.at(0));
        if(gServer.hasPlayer(gServer.getSvClient())) {
            gServer.getPlayer(gServer.getSvClient())->setAcceleration(value);
        }
    } else {
        if(gServer.hasPlayer(gServer.getSvClient())) {
            console->print(Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->getAcceleration()));
        } else {
            console->print("you are not a player");
        }
    }
}
void setAirControl(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float value = Console::Variable::convertFromString<float>(tokens.at(0));
        if(gServer.hasPlayer(gServer.getSvClient())) {
            gServer.getPlayer(gServer.getSvClient())->setAirControl(value);
        }
    } else {
        if(gServer.hasPlayer(gServer.getSvClient())) {
            console->print(Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->getAirControl()));
        } else {
            console->print("you are not a player");
        }
    }
}
void setJumpPower(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float value = Console::Variable::convertFromString<float>(tokens.at(0));
        if(gServer.hasPlayer(gServer.getSvClient())) {
            gServer.getPlayer(gServer.getSvClient())->setJumpPower(value);
        }
    } else {
        if(gServer.hasPlayer(gServer.getSvClient())) {
            console->print(Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->getJumpPower()));
        } else {
            console->print("you are not a player");
        }
    }
}
void setGPosition(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 3) {
        glm::vec3 value(Console::Variable::convertFromString<float>(tokens.at(0)),
                        Console::Variable::convertFromString<float>(tokens.at(1)),
                        Console::Variable::convertFromString<float>(tokens.at(2)));
        for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
            gServer.state()->getPlayer(i)->transform()->setPosition(value);
        }
    } else {
        console->print("invalid args");
    }
}
void setGVelocity(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 3) {
        glm::vec3 value(Console::Variable::convertFromString<float>(tokens.at(0)),
                        Console::Variable::convertFromString<float>(tokens.at(1)),
                        Console::Variable::convertFromString<float>(tokens.at(2)));
        for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
            gServer.state()->getPlayer(i)->setVelocity(value);
        }
    } else {
        console->print("invalid args");
    }
}
void setGMaxVelocity(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float value = Console::Variable::convertFromString<float>(tokens.at(0));
        for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
            gServer.state()->getPlayer(i)->setMaxVelocity(value);
        }
    } else {
        console->print("invalid args");
    }
}
void setGAcceleration(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float value = Console::Variable::convertFromString<float>(tokens.at(0));
        for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
            gServer.state()->getPlayer(i)->setAcceleration(value);
        }
    } else {
        console->print("invalid args");
    }
}
void setGAirControl(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float value = Console::Variable::convertFromString<float>(tokens.at(0));
        for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
            gServer.state()->getPlayer(i)->setAirControl(value);
        }
    } else {
        console->print("invalid args");
    }
}
void setGJumpPower(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const float value = Console::Variable::convertFromString<float>(tokens.at(0));
        for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
            gServer.state()->getPlayer(i)->setJumpPower(value);
        }
    } else {
        console->print("invalid args");
    }
}
void setHealth(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const int value = Console::Variable::convertFromString<int>(tokens.at(0));
        if(gServer.hasPlayer(gServer.getSvClient())) {
            gServer.getPlayer(gServer.getSvClient())->setHealth(value);
        }
    } else {
        if(gServer.hasPlayer(gServer.getSvClient())) {
            console->print(Console::Variable::convertToString(gServer.getPlayer(gServer.getSvClient())->getHealth()));
        } else {
            console->print("you are not a player");
        }
    }
}
void setGHealth(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() == 1) {
        const int value = Console::Variable::convertFromString<int>(tokens.at(0));
        for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
            gServer.state()->getPlayer(i)->setHealth(value);
        }
    } else {
        console->print("invalid args");
    }
}
void gKill(const std::string& args, Console* console) {
    for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
        gServer.state()->getPlayer(i)->die();
    }
}
void gLive(const std::string& args, Console* console) {
    for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
        gServer.state()->getPlayer(i)->respawn();
    }
}
void kill(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() >= 1) {
        const std::string requestedPlayer = tokens.at(0);
        if(gServer.state()->hasPlayer(requestedPlayer)) {
            gServer.state()->getPlayer(requestedPlayer)->die();
        } else {
            console->print("no player named '" + requestedPlayer + "'");
        }
    } else {
        gServer.getPlayer(gServer.getSvClient())->die();
    }
}
void live(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() >= 1) {
        const std::string requestedPlayer = tokens.at(0);
        if(gServer.state()->hasPlayer(requestedPlayer)) {
            gServer.state()->getPlayer(requestedPlayer)->respawn();
        } else {
            console->print("no player named '" + requestedPlayer + "'");
        }
    } else {
        gServer.getPlayer(gServer.getSvClient())->respawn();
    }
}
void team(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() >= 3) {
        if(gServer.hasPlayer(gServer.getSvClient())) {
            glm::vec3 newTeam;
            newTeam.x = Console::Variable::convertFromString<float>(tokens.at(0));
            newTeam.y = Console::Variable::convertFromString<float>(tokens.at(1));
            newTeam.z = Console::Variable::convertFromString<float>(tokens.at(2));
            gServer.getPlayer(gServer.getSvClient())->setTeam(newTeam);
        } else {
            console->print("you are not a player");
        }
    } else {
        if(gServer.hasPlayer(gServer.getSvClient())) {
            glm::vec3 team = gServer.getPlayer(gServer.getSvClient())->getTeam();
            std::string sTeam;
            sTeam += Console::Variable::convertToString(team.x) + " " +
                     Console::Variable::convertToString(team.y) + " " +
                     Console::Variable::convertToString(team.z);
            console->print("your team is " + sTeam);
        }
    }
}
void weaponDamage(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() > 0) {
        const float damage = Console::Variable::convertFromString<float>(tokens.at(0));
        for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
            gServer.state()->getPlayer(i)->weapon()->setDamage(damage);
        }
    } else {
        console->print("invalid args");
    }
}
void weaponKickback(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() > 0) {
        const float kickback = Console::Variable::convertFromString<float>(tokens.at(0));
        for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
            gServer.state()->getPlayer(i)->weapon()->setKickback(kickback);
        }
    } else {
        console->print("invalid args");
    }
}
void weaponCooldown(const std::string& args, Console* console) {
    const std::vector<std::string> tokens = Console::tokenize(args);
    if(tokens.size() > 0) {
        const float cooldown = Console::Variable::convertFromString<float>(tokens.at(0));
        for(unsigned int i=0;i<gServer.state()->getPlayers().size();i++) {
            gServer.state()->getPlayer(i)->weapon()->setCooldown(cooldown);
        }
    } else {
        console->print("invalid args");
    }
}
void addBot(const std::string& args, Console* console) {
    gServer.state()->addPlayer("Bot");
}
int main() {
    try {
        srand(time(nullptr));

        std::cout << "ace of blades server\n";

        // init console
        Console console;
        console.addFunction("bind", "server port", "port", &bind);
        console.addFunction("tickrate", "server tickrate", "ms", &setTickrate);
        console.addFunction("timeout", "server timeout", "ms", &setTimeout);

        console.addFunction("gravity", "global gravity", "x y z", &setGravity);
        console.addFunction("friction", "global friction", "float", &setFriction);
        console.addFunction("airfriction", "global air friction", "float", &setAirFriction);
        console.addFunction("restitution", "global restitution", "float", &setRestitution);

        console.addFunction("position", "personal position", "x y z", &setPosition);
        console.addFunction("velocity", "personal velocity", "x y z", &setVelocity);
        console.addFunction("maxvelocity", "personal maximum velocity", "float", &setMaxVelocity);
        console.addFunction("acceleration", "personal acceleration", "float", &setAcceleration);
        console.addFunction("aircontrol", "personal air control", "float", &setAirControl);
        console.addFunction("jumppower", "personal jump power", "float", &setJumpPower);
        console.addFunction("health", "personal health", "int", &setHealth);

        console.addFunction("g_position", "global position", "x y z", &setGPosition);
        console.addFunction("g_velocity", "global velocity", "x y z", &setGVelocity);
        console.addFunction("g_maxvelocity", "global velocity limit", "float", &setGMaxVelocity);
        console.addFunction("g_acceleration", "global acceleration", "float", &setGAcceleration);
        console.addFunction("g_aircontrol", "global air control", "float", &setGAirControl);
        console.addFunction("g_jumppower", "global jump power", "float", &setGJumpPower);
        console.addFunction("g_health", "global health", "int", &setGHealth);

        console.addFunction("g_kill", "kills everyone", "", &gKill);
        console.addFunction("g_live", "respawn", "", &gLive);

        console.addFunction("kill", "kills a player. leave blank to suicide", "player", &kill);
        console.addFunction("live", "respawn", "player", &live);

        console.addFunction("addbot", "add an ai controlled player", "", &addBot);
        console.addFunction("team", "set your team color", "r g b", &team);

        console.addFunction("weapon_damage", "set the global weapon damage", "float", &weaponDamage);
        console.addFunction("weapon_cooldown", "set the global weapon cooldown", "float", &weaponCooldown);
        console.addFunction("weapon_kickback", "set the global weapon damage", "float", &weaponKickback);

        // init server
        unsigned short port = 8008;
        while(!gServer.bindToPort(port)) {
            std::cout << "bind to port:";
            std::cin >> port;
        }
        gServer.setTickrate(gServer.getTickrate());
        gServer.setTimeout(gServer.getTimeout());
        gServer.setConsole(&console);

        std::cout << "bound to port " << port << "\n";
        std::cout << "server started\n";
        bool running = true;
        while(running) {
            gServer.update();
            if(kbhit()) {
                char ch = getch();

                if(ch == 27) {  // esc
                    running = false;
                }
            }
        }
        gServer.disconnect();
        std::cout << "exiting...\n";
    } catch(std::runtime_error& e) {
        std::cout << e.what() << std::endl;
        std::cout << "enter anything to exit\n";
        std::string nothing;
        std::cin >> nothing;
    }
    return 0;
}

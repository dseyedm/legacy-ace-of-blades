#version 120
uniform sampler2D s3d_model_diffuse;
varying vec2 custom_uv_frag;
void main() {
	gl_FragColor = texture2D(s3d_model_diffuse, custom_uv_frag * 25);
}

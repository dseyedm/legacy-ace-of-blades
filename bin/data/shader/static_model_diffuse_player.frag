#version 120
uniform sampler2D s3d_model_diffuse;
uniform vec3 player_team;
varying vec2 custom_uv_frag;
void main() {
	gl_FragColor = texture2D(s3d_model_diffuse, custom_uv_frag) * vec4(player_team, 1.0);
}

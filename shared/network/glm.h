#ifndef AOB_NETWORK_GLM_H
#define AOB_NETWORK_GLM_H

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <SFML/Network.hpp>

sf::Packet& operator << (sf::Packet& packet, glm::vec3& m);
sf::Packet& operator >> (sf::Packet& packet, glm::vec3& m);

sf::Packet& operator << (sf::Packet& packet, glm::quat& m);
sf::Packet& operator >> (sf::Packet& packet, glm::quat& m);

#endif // AOB_NETWORK_GLM_H

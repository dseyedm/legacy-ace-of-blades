#include "glm.h"

sf::Packet& operator << (sf::Packet& packet, glm::vec3& m) {
    packet << m.x << m.y << m.z;
    return packet;
}
sf::Packet& operator >> (sf::Packet& packet, glm::vec3& m) {
    float x, y, z;
    packet >> x >> y >> z;
    m = glm::vec3(x, y, z);
    return packet;
}

sf::Packet& operator << (sf::Packet& packet, glm::quat& m) {
    packet << m.x << m.y << m.z << m.w;
    return packet;
}
sf::Packet& operator >> (sf::Packet& packet, glm::quat& m) {
    float x, y, z, w;
    packet >> x >> y >> z >> w;
    m = glm::quat(w, x, y, z);
    return packet;
}

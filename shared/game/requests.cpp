#include "requests.h"

game::Requests::Requests() {
    mRight = false, mLeft = false, mForward = false, mBack = false, mJump = false, mCrouch = false, mFire = false, mAltFire = false;
}

void game::Requests::set(Requests& r) {
    mRight       = r.mRight;
    mLeft        = r.mLeft;
    mForward     = r.mForward;
    mBack        = r.mBack;
    mJump        = r.mJump;
    mCrouch      = r.mCrouch;
    mFire        = r.mFire;
    mAltFire     = r.mAltFire;
    mOrientation = r.mOrientation;
}

sf::Packet& operator << (sf::Packet& packet, game::Requests& m) {
    packet << m.mRight << m.mLeft << m.mForward << m.mBack << m.mJump << m.mCrouch << m.mFire << m.mAltFire;
    packet << m.mOrientation;
    return packet;
}
sf::Packet& operator >> (sf::Packet& packet, game::Requests& m) {
    packet >> m.mRight >> m.mLeft >> m.mForward >> m.mBack >> m.mJump >> m.mCrouch >> m.mFire >> m.mAltFire;
    packet >> m.mOrientation;
    return packet;
}

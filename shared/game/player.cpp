#include "player.h"

game::Player::Player() {
    setAcceleration(5.f);
    setMaxVelocity(1.f);
    setAirControl(0.3f);
    setJumpPower(3.f);
    setHealth(100);
    setTeam(glm::vec3(1));

    weapon()->setOwner(this);
}

const std::string& game::Player::getName() {
    return mName;
}
void game::Player::setName(const std::string& name) {
    mName = name;
}

const glm::vec3& game::Player::getVelocity() {
    return mVelocity;
}
void game::Player::setVelocity(const glm::vec3& velocity) {
    mVelocity = velocity;
}
const float game::Player::getMaxVelocity() {
    return mMaxVelocity;
}
void game::Player::setMaxVelocity(const float maxVelocity) {
    mMaxVelocity = maxVelocity;
}
const float game::Player::getAcceleration() {
    return mAcceleration;
}
void game::Player::setAcceleration(const float acceleration) {
    mAcceleration = acceleration;
}
const float game::Player::getAirControl() {
    return mAirControl;
}
void game::Player::setAirControl(const float control) {
    mAirControl = control;
}
const float game::Player::getJumpPower() {
    return mJumpPower;
}
void game::Player::setJumpPower(const float jump) {
    mJumpPower = jump;
}
const int game::Player::getHealth() {
    return mHealth;
}
void game::Player::setHealth(const int health) {
    mHealth = health;
}
const glm::vec3& game::Player::getTeam() {
    return mTeam;
}
void game::Player::setTeam(const glm::vec3& team) {
    mTeam = team;
}

game::Raygun* game::Player::weapon() {
    return &mWeapon;
}

const bool game::Player::alive() {
    return (getHealth() > 0);
}
void game::Player::die() {
    setHealth(0);
    events()->mDied = true;
}
void game::Player::respawn() {
    setHealth(100);
    events()->mRespawned = true;
}
void game::Player::damage(const int amount) {
    if(amount != 0) {
        if(alive()) {
            if(getHealth() - amount > 0) {
                setHealth(getHealth() - amount);
                events()->mHurt = true;
            } else {
                die();
            }
        }
    }
}

s3d::Transform* game::Player::transform() {
    return &mTransform;
}
game::Requests* game::Player::requests() {
    return &mRequests;
}
game::Events* game::Player::events() {
    return &mEvents;
}

sf::Packet& operator << (sf::Packet& packet, game::Player& m) {
    packet << m.getName();
    packet << (glm::vec3&)m.getVelocity();
    packet << m.getMaxVelocity();
    packet << m.getAcceleration();
    packet << m.getAirControl();
    packet << m.getJumpPower();
    packet << m.getHealth();
    packet << (glm::vec3&)m.getTeam();
    packet << *m.weapon();
    packet << *m.transform();
    packet << *m.requests();
    packet << *m.events();
    return packet;
}
sf::Packet& operator >> (sf::Packet& packet, game::Player& m) {
    std::string name;
    glm::vec3 velocity;
    float maxVelocity;
    float acceleration;
    float aircontrol;
    float jumppower;
    int health;
    glm::vec3 team;

    packet >> name;
    packet >> velocity;
    packet >> maxVelocity;
    packet >> acceleration;
    packet >> aircontrol;
    packet >> jumppower;
    packet >> health;
    packet >> team;
    packet >> *m.weapon();
    packet >> *m.transform();
    packet >> *m.requests();
    packet >> *m.events();

    m.setName(name);
    m.setVelocity(velocity);
    m.setMaxVelocity(maxVelocity);
    m.setAcceleration(acceleration);
    m.setAirControl(aircontrol);
    m.setJumpPower(jumppower);
    m.setHealth(health);
    m.setTeam(team);
    return packet;
}

sf::Packet& operator << (sf::Packet& packet, s3d::Transform& m) {
    packet << (glm::vec3&)m.getPosition();
    packet << (glm::vec3&)m.getScale();
    packet << (glm::quat&)m.getOrientation();
    return packet;
}
sf::Packet& operator >> (sf::Packet& packet, s3d::Transform& m) {
    glm::vec3 pos;
    glm::vec3 scale;
    glm::quat orientation;
    packet >> pos;
    packet >> scale;
    packet >> orientation;
    m.setPosition(pos);
    m.setScale(scale);
    m.setOrientation(orientation);
    return packet;
}

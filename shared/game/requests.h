#ifndef AOB_REQUESTS_H
#define AOB_REQUESTS_H

#include <glm/glm.hpp>
#include <SFML/Network.hpp>

#include "../network/glm.h"

namespace game {
class Requests {
public:
    Requests();

    void set(Requests& r);

    bool mRight, mLeft, mForward, mBack, mJump, mCrouch, mFire, mAltFire;
    glm::quat mOrientation;
    // team requests here too
};
}

sf::Packet& operator << (sf::Packet& packet, game::Requests& m);
sf::Packet& operator >> (sf::Packet& packet, game::Requests& m);

#endif // AOB_REQUESTS_H

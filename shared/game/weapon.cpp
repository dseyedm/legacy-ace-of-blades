#include "weapon.h"
#include "player.h"

game::Weapon::Weapon() {
    setDamage(25);
    setKickback(3);
    setCooldown(1);
    setOwner(nullptr);
}

const bool game::Weapon::ready() const {
    return getCooldownTimer() >= getCooldown();
}

const float game::Weapon::getDamage() const {
    return mDamage;
}
void game::Weapon::setDamage(const float damage) {
    mDamage = damage;
}
const float game::Weapon::getKickback() const {
    return mKickback;
}
void game::Weapon::setKickback(const float kickback) {
    mKickback = kickback;
}
const float game::Weapon::getCooldown() const {
    return mCooldown;
}
void game::Weapon::setCooldown(const float cooldown) {
    mCooldown = cooldown;
}
const float game::Weapon::getCooldownTimer() const {
    return mCooldownTimer.getElapsedTime().asSeconds();
}
void game::Weapon::restartCooldownTimer() {
    mCooldownTimer.restart();
}

void game::Weapon::setOwner(Player* const owner) {
    mOwner = owner;
}
game::Player* const game::Weapon::getOwner() const {
    return mOwner;
}
const bool game::Weapon::hasOwner() {
    return (mOwner != nullptr);
}

game::Raygun::Raygun() {
    setDamage(15);
    setKickback(4);
    setCooldown(1.2);
}

void game::Raygun::fire(Player* const target) {
    if(hasOwner()) {
        s3dc::Sphere colTarget;
        colTarget.mPosition = target->transform()->getPosition();
        colTarget.mRadius = target->transform()->getScale().x / 2;

        s3dc::Capsule colBullet;
        colBullet.mPosition = getOwner()->transform()->getPosition();
        colBullet.mDirection = getOwner()->transform()->forward(false);
        colBullet.mRadius = 0.2;
        if(s3dc::sphereCapsule(colTarget, colBullet)) {
            target->damage(getDamage());
            if(!target->alive()) {
                target->events()->mAssailant = getOwner()->getName();
            }
            target->setVelocity(target->getVelocity() + getOwner()->transform()->forward(false) * getKickback());
        }
    }
}

void game::Raygun::kickback() {
    if(hasOwner()) {
        getOwner()->setVelocity(getOwner()->getVelocity() - getOwner()->transform()->forward(false) * getKickback());
    }
}

sf::Packet& game::operator << (sf::Packet& packet, game::Weapon& m) {
    packet << m.getDamage();
    packet << m.getKickback();
    packet << m.getCooldown();
    return packet;
}
sf::Packet& game::operator >> (sf::Packet& packet, game::Weapon& m) {
    float damage;
    float kickback;
    float cooldown;
    packet >> damage;
    packet >> kickback;
    packet >> cooldown;
    m.setDamage(damage);
    m.setKickback(kickback);
    m.setCooldown(cooldown);
    return packet;
}

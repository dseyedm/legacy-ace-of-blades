#ifndef AOB_PLAYER_H
#define AOB_PLAYER_H

#include "requests.h"
#include "events.h"
#include "weapon.h"
#include "../network/glm.h"
#include <s3d/transform.h>

namespace game {
class Player {
public:
    Player();

    const std::string& getName();
    void setName(const std::string& name);

    const glm::vec3& getVelocity();
    void setVelocity(const glm::vec3& velocity);
    const float getMaxVelocity();
    void setMaxVelocity(const float maxVelocity);
    const float getAcceleration();
    void setAcceleration(const float acceleration);
    const float getAirControl();
    void setAirControl(const float control);
    const float getJumpPower();
    void setJumpPower(const float jump);
    const int getHealth();
    void setHealth(const int health);
    const glm::vec3& getTeam();
    void setTeam(const glm::vec3& team);

    Raygun* weapon();

    const bool alive();
    void die();
    void respawn();
    void damage(const int amount);

    s3d::Transform* transform();
    game::Requests* requests();
    game::Events* events();
private:
    std::string mName;

    // movement and position
    glm::vec3 mVelocity;
    float mMaxVelocity;
    float mAcceleration;
    float mAirControl;
    float mJumpPower;
    glm::vec3 mTeam;
    s3d::Transform mTransform;
    game::Requests mRequests;

    // game events
    game::Events mEvents;

    // game data
    int mHealth;
    Raygun mWeapon;
};
}

sf::Packet& operator << (sf::Packet& packet, game::Player& m);
sf::Packet& operator >> (sf::Packet& packet, game::Player& m);

sf::Packet& operator << (sf::Packet& packet, s3d::Transform& m);
sf::Packet& operator >> (sf::Packet& packet, s3d::Transform& m);

#endif // AOB_PLAYER_H

#include "state.h"

game::State::State() {
    setGravity(glm::vec3(0, -9.81, 0));
    setFriction(5);
    setAirFriction(3);
    setRestitution(0.45);
}
void game::State::update(float dt) {
    if(dt > 1) {
        dt = 1;
    }

    for(unsigned int i=0;i<mPlayers.size();i++) {
        game::Player* player = mPlayers.at(i);

        player->transform()->setOrientation(player->requests()->mOrientation);
        if(player->alive()) {
            updateMovement(player, dt);
            updateAttack(player, dt);
        } else {
            updateDeath(player, dt);
        }
        updatePhysics(player, dt);
    }
}

game::Player* game::State::addPlayer(const std::string& requestedName) {
    mPlayers.push_back(new game::Player);
    mPlayers.back()->events()->mRespawned = true;
    mPlayers.back()->setName(generateName(requestedName));
    mPlayers.back()->transform()->setPosition(glm::vec3(0, 2.5, 0));
    mPlayers.back()->transform()->setScale(glm::vec3(0.5));
    mPlayers.back()->setAcceleration(28);
    mPlayers.back()->setMaxVelocity(9);
    mPlayers.back()->setAirControl(0.8f);
    mPlayers.back()->setJumpPower(3.5f);
    mPlayers.back()->setHealth(100);
    return mPlayers.back();
}
const bool game::State::hasPlayer(const std::string& name) {
    return (getPlayer(name) != nullptr);
}
game::Player* game::State::getPlayer(const std::string& name) {
    for(unsigned int i=0;i<mPlayers.size();i++) {
        if(name == mPlayers.at(i)->getName()) {
            return mPlayers.at(i);
        }
    }
    return nullptr;
}
game::Player* game::State::getPlayer(const int i) {
    return mPlayers.at(i);
}
const std::vector<game::Player*>& game::State::getPlayers() {
    return mPlayers;
}
void game::State::removePlayer(const int i) {
    delete mPlayers.at(i);
    mPlayers.at(i) = mPlayers.back();
    mPlayers.pop_back();
}
void game::State::removePlayer(const std::string& name) {
    for(unsigned int i=0;i<mPlayers.size();i++) {
        if(mPlayers.at(i)->getName() == name) {
            removePlayer(i);
        }
    }
}
void game::State::removeAllPlayers() {
    for(unsigned int i=0;i<mPlayers.size();i++) {
        removePlayer(i);
    }
}
void game::State::clearEvents() {
    for(unsigned int i=0;i<mPlayers.size();i++) {
        mPlayers.at(i)->events()->clear();
    }
}

const std::string game::State::generateName(const std::string& requested) {
    // use the first word to name him
    std::vector<std::string> tokens = Console::tokenize(requested);
    std::string tokenName;
    if(!tokens.empty()) {
        tokenName = tokens.at(0);
    } else {
        tokenName = "Player";
    }

    bool chosen = false;
    std::string calculatedName;
    int number = 1;
    while(!chosen) {
        chosen = true;
        calculatedName = tokenName;
        if(number >= 2) {
            calculatedName += Console::Variable::convertToString(number);
        }
        for(unsigned int i=0;i<mPlayers.size();i++) {
            if(calculatedName == mPlayers.at(i)->getName()) {
                chosen = false;
                i = mPlayers.size();    // break out of loop
            }
        }
        number++;
    }
    return calculatedName;
}
void game::State::set(game::State& state) {
    *this = state;
}

const float game::State::getFriction() {
    return mFriction;
}
void game::State::setFriction(const float friction) {
    mFriction = friction;
}
const float game::State::getAirFriction() {
    return mAirFriction;
}
void game::State::setAirFriction(const float airFriction) {
    mAirFriction = airFriction;
}
const glm::vec3& game::State::getGravity() {
    return mGravity;
}
void game::State::setGravity(const glm::vec3& gravity) {
    mGravity = gravity;
}
const float game::State::getRestitution() {
    return mRestitution;
}
void game::State::setRestitution(const float restitution) {
    mRestitution = restitution;
}

void game::State::updateMovement(game::Player* player, const float dt) {
    glm::vec3 position = player->transform()->getPosition();
    glm::vec3 velocity = player->getVelocity();
    const glm::vec3 scale = player->transform()->getScale();

    glm::vec3 movement(0);
    float jump = 0;
    if(player->requests()->mForward) {
        movement += s3dc::normalize(glm::vec3(1, 0, 1) * player->transform()->forward(false));
    }
    if(player->requests()->mBack) {
        movement -= s3dc::normalize(glm::vec3(1, 0, 1) * player->transform()->forward(false));
    }
    if(player->requests()->mRight) {
        movement += s3dc::normalize(glm::vec3(1, 0, 1) * player->transform()->right(false));
    }
    if(player->requests()->mLeft) {
        movement -= s3dc::normalize(glm::vec3(1, 0, 1) * player->transform()->right(false));
    }
    if(player->requests()->mJump && position.y <= scale.y / 2) {
        jump = player->getJumpPower();
        player->events()->mJumped = true;
    }
    if(player->requests()->mCrouch) {
        ;
    }

    // apply acceleration
    glm::vec3 acceleration;
    acceleration = player->getAcceleration() * s3dc::normalize(movement);
    if(position.y > scale.y / 2) {
        acceleration.x *= player->getAirControl();
        acceleration.z *= player->getAirControl();
    }
    velocity += acceleration * dt;
    velocity.y += jump;

    // set the player variables
    player->transform()->setPosition(position);
    player->setVelocity(velocity);
}
void game::State::updateAttack(game::Player* player, const float dt) {
    // main fire
    if(player->requests()->mFire) {
        if(player->weapon()->ready()) {
            player->events()->mFired = true;

            for(unsigned int i = 0; i < mPlayers.size(); ++i) {
                game::Player* target = mPlayers.at(i);
                if(target != player) {
                    player->weapon()->fire(target);
                }
            }
            player->weapon()->kickback();
            player->weapon()->restartCooldownTimer();
        }
    }
}
void game::State::updateDeath(game::Player* player, const float dt) {
    player->transform()->setOrientation(glm::quat());
    player->transform()->rotate(player->transform()->getRotation().y, glm::vec3(0,1,0));
    player->transform()->rotate(-90, player->transform()->forward(false));
}
void game::State::updatePhysics(game::Player* player, const float dt) {
    glm::vec3 position = player->transform()->getPosition();
    glm::vec3 velocity = player->getVelocity();
    const glm::vec3 scale = player->transform()->getScale();
    const float maxVelocity = player->getMaxVelocity();

    // limit the players velocity
    const float flatVelocity = (velocity.x * velocity.x + velocity.z * velocity.z);
    if(flatVelocity > (maxVelocity * maxVelocity)) {
        velocity.x = maxVelocity * velocity.x / std::sqrt(flatVelocity);
        velocity.z = maxVelocity * velocity.z / std::sqrt(flatVelocity);
    }

    { // apply friction
        const glm::vec3 direction = s3dc::normalize(velocity * glm::vec3(1, 0, 1));
        float friction = 0;
        // test if player is grounded
        if(position.y <= scale.y / 2) {
            friction = getFriction();
        } else {
            friction = getAirFriction();
        }
        // if the magnitude of the velocity is less than friction
        if(flatVelocity <= (friction * dt) * (friction * dt)) {
            velocity.x = 0;
            velocity.z = 0;
        } else {
            velocity += friction * -direction * dt;
        }
    }

    // apply gravity
    velocity += getGravity() * dt;

    // calculate his new position
    position += velocity * dt;

    const glm::vec3 room(25, 2.5, 25);
    // if the new position is colliding with the positive x
    if(position.x + player->transform()->getScale().x / 2 > room.x / 2) {
        position.x = room.x / 2 - player->transform()->getScale().x / 2;
        velocity.x *= -getRestitution();
    }
    // if the new position is colliding with the negative x
    if(position.x - player->transform()->getScale().x / 2 < -room.x / 2) {
        position.x = player->transform()->getScale().x / 2 - room.x / 2;
        velocity.x *= -getRestitution();
    }
    // if the new position is colliding with the positive z
    if(position.z + player->transform()->getScale().z / 2 > room.z / 2) {
        position.z = room.z / 2 - player->transform()->getScale().z / 2;
        velocity.z *= -getRestitution();
    }
    // if the new position is colliding with the negative z
    if(position.z - player->transform()->getScale().z / 2 < -room.z / 2) {
        position.z = player->transform()->getScale().z / 2 - room.z / 2;
        velocity.z *= -getRestitution();
    }
    // if the new position is colliding with the negative y
    if(position.y < player->transform()->getScale().y / 2) {
        position.y = player->transform()->getScale().y / 2;
        velocity.y *= -getRestitution();
    }

    // set the player variables
    player->transform()->setPosition(position);
    player->setVelocity(velocity);
}

sf::Packet& operator << (sf::Packet& packet, game::State& m) {
    // write
    int size = m.getPlayers().size();
    packet << size;
    for(int i=0;i<size;i++) {
        packet << *m.getPlayer(i);
    }
    packet << m.getFriction();
    packet << m.getAirFriction();
    packet << (glm::vec3&)m.getGravity();
    packet << m.getRestitution();
    return packet;
}
sf::Packet& operator >> (sf::Packet& packet, game::State& m) {
    // read
    int size = 0;
    packet >> size;
    m.removeAllPlayers();
    for(int i=0;i<size;i++) {
        game::Player* newPlayer = m.addPlayer("");
        packet >> *newPlayer;
    }

    float friction;
    float airFriction;
    glm::vec3 gravity;
    float restitution;

    packet >> friction;
    packet >> airFriction;
    packet >> gravity;
    packet >> restitution;

    m.setFriction(friction);
    m.setAirFriction(airFriction);
    m.setGravity(gravity);
    m.setRestitution(restitution);

    return packet;
}

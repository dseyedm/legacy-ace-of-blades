#ifndef S3D_CONSOLE_H
#define S3D_CONSOLE_H

#include "console/SFMLConsole.h"
#include "s3d/input.h"

namespace s3d {
class Console : public SFMLConsole {
public:
    Console();

    void update(sf::Event* event);
    s3d::Input* input();
private:
    void setOpenCloseChar(const char ch);

    s3d::Input mInput;
    sf::Clock mBackspaceClock;
    char mLastChar;
    char mOpenClose;
};
}

#endif // S3D_CONSOLE_H

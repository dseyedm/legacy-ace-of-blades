#include "s3dconsole.h"

s3d::Console::Console() {
    setOpenCloseChar('`');
}

void s3d::Console::update(sf::Event* event) {
    if(event->type == sf::Event::TextEntered) {
        const char newChar = static_cast<char>(event->text.unicode);
        if(newChar == mOpenClose && mLastChar != newChar) {
            setVisible(!isVisible());
            input()->setEnabled(isVisible());
        }
        if(newChar != mLastChar && newChar != mOpenClose) {
            if(input()->isEnabled()) {
                inputChar(newChar);
            }
        }
        mLastChar = newChar;
    } else {
        mLastChar = 0;
    }
    const int scrollAmount = 2;
    if(input()->keyTapped(sf::Keyboard::Key::PageUp)) {
        scroll(-scrollAmount);
    }
    if(input()->keyTapped(sf::Keyboard::Key::PageDown)) {
        scroll(scrollAmount);
    }
    if(input()->keyTapped(sf::Keyboard::Key::BackSpace)) {
        inputBackspace();
        mBackspaceClock.restart();
    } else if(input()->keyPressed(sf::Keyboard::Key::BackSpace)) {
        if(mBackspaceClock.getElapsedTime().asSeconds() > 0.15) {
            mBackspaceClock.restart();
            inputBackspace();
        }
    }
    if(input()->keyTapped(sf::Keyboard::Key::Return)) {
        execute();
    }
    if(input()->keyTapped(sf::Keyboard::Key::Up)) {
        recallHistory(-1);
    }
    if(input()->keyTapped(sf::Keyboard::Key::Down)) {
        recallHistory(1);
    }
}

void s3d::Console::setOpenCloseChar(const char ch) {
    mOpenClose = ch;
}

s3d::Input* s3d::Console::input() {
    return &mInput;
}

#ifndef AOB_PLAYER_INPUT_H
#define AOB_PLAYER_INPUT_H

#include "../world/world.h"

class PlayerInput {
public:
    PlayerInput();
    void update(s3d::Window* window, s3d::Camera* camera, const float dt);

    void setWorld(World* world);
    World* getWorld();
    const bool hasWorld();

    s3d::Input* input();
private:
    World* mWorld;
    s3d::Input mInput;
};

#endif // AOB_PLAYER_INPUT_H

#include "input.h"

PlayerInput::PlayerInput() {
    setWorld(nullptr);
    input()->setEnabled(true);
}
void PlayerInput::update(s3d::Window* window, s3d::Camera* camera, const float dt) {
    if(hasWorld() && getWorld()->hasLocalPlayer()) {
        game::Player* local = getWorld()->getLocalPlayer();
        camera->transform()->setPosition(local->transform()->getPosition());

        // issue movements
        local->requests()->mForward = input()->keyPressed(sf::Keyboard::Key::W);
        local->requests()->mBack    = input()->keyPressed(sf::Keyboard::Key::S);
        local->requests()->mLeft    = input()->keyPressed(sf::Keyboard::Key::A);
        local->requests()->mRight   = input()->keyPressed(sf::Keyboard::Key::D);
        local->requests()->mJump    = input()->keyPressed(sf::Keyboard::Key::Space);
        local->requests()->mCrouch  = input()->keyPressed(sf::Keyboard::Key::LControl);
        local->requests()->mFire    = input()->mousePressed(sf::Mouse::Button::Left);
        local->requests()->mAltFire = input()->mousePressed(sf::Mouse::Button::Right);

        // issue orientation
        if(input()->isEnabled()) {
            camera->look(window, dt);
        }
        local->requests()->mOrientation = glm::inverse(camera->transform()->getOrientation());

        if(local->events()->mDied) {
            camera->transform()->rotate(90, glm::vec3(0, 0.2, 1));
        }
        if(local->events()->mRespawned) {
            camera->transform()->setOrientation(glm::quat());
        }
    }
}
void PlayerInput::setWorld(World* world) {
    mWorld = world;
}
World* PlayerInput::getWorld() {
    return mWorld;
}
const bool PlayerInput::hasWorld() {
    return (getWorld() != nullptr);
}

s3d::Input* PlayerInput::input() {
    return &mInput;
}

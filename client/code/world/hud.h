#ifndef AOB_HUD_H
#define AOB_HUD_H

#include "game/state.h"
#include "s3d/input.h"

#include <SFML/Graphics.hpp>

class World;
class Hud {
public:
    Hud();

    void update();
    void render(sf::RenderTarget* target);

    void setTargetFont(sf::Font* font);
    sf::Font* getTargetFont();
    const bool hasTargetFont();

    void setHealthFont(sf::Font* font);
    sf::Font* getHealthFont();
    const bool hasHealthFont();

    void setNotificationFont(sf::Font* font);
    sf::Font* getNotificationFont();
    const bool hasNotificationFont();

    void setWorld(World* world);
    World* getWorld();
    const bool hasWorld();

    void setNotification(const std::string& notification, const float time = 2);
private:
    sf::Font* mTargetFont;
    sf::Font* mHealthFont;
    sf::Font* mNotificationFont;
    World* mWorld;

    std::string mTarget;
    std::string mHealth;
    int mLastHealth;
    std::string mNotification;
    float mNotificationTime;
    sf::Clock mNotificationClock;

    sf::RectangleShape mCrosshairSprite;
    sf::Texture mCrosshair;
};

#endif // AOB_HUD_H

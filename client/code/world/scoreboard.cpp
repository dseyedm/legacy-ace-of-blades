#include "scoreboard.h"
#include "world.h"

Scoreboard::Scoreboard() {
    setFont(nullptr);
    mRender = false;
}

void Scoreboard::update() {
    if(hasWorld()) {
        // if a new player has joined, reset the scoreboard
        if(mNames.size() != getWorld()->state()->getPlayers().size()) {
            mNames.clear();
            mNames.resize(getWorld()->state()->getPlayers().size());
        }
        for(unsigned int i=0;i<getWorld()->state()->getPlayers().size();i++) {
            mNames.at(i) = getWorld()->state()->getPlayers().at(i)->getName();
        }
        mRender = input()->keyPressed(sf::Keyboard::Key::Tab);
    }
}
void Scoreboard::render(sf::RenderTarget* target) {
    if(hasFont()) {
        if(mRender) {
            sf::Text tNames;
            tNames.setFont(*mFont);
            for(unsigned int i=0;i<mNames.size();i++) {
                if(hasWorld() && getWorld()->hasLocalPlayer()) {
                    if(mNames.at(i) == getWorld()->getLocalPlayer()->getName()) {
                        tNames.setColor(sf::Color::Red);
                    } else {
                        tNames.setColor(sf::Color::White);
                    }
                }
                tNames.setPosition(sf::Vector2f(0, tNames.getCharacterSize() * (1+i)));
                tNames.setString(mNames.at(i));
                target->draw(tNames);
            }
        }
    }
}

void Scoreboard::setFont(sf::Font* font) {
    mFont = font;
}
sf::Font* Scoreboard::getFont() {
    return mFont;
}
const bool Scoreboard::hasFont() {
    return (getFont() != nullptr);
}

void Scoreboard::setWorld(World* world) {
    mWorld = world;
}
World* Scoreboard::getWorld() {
    return mWorld;
}
const bool Scoreboard::hasWorld() {
    return (getWorld() != nullptr);
}

s3d::Input* Scoreboard::input() {
    return &mInput;
}

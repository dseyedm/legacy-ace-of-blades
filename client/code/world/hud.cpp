#include "hud.h"
#include "world.h"

Hud::Hud() {
    setTargetFont(nullptr);
    setHealthFont(nullptr);
    mLastHealth = 0;

    if(!mCrosshair.loadFromFile("data/texture/crosshair.png")) {
        throw std::runtime_error("failed to load texture");
    }
    mCrosshairSprite.setTexture(&mCrosshair);
    mCrosshairSprite.setSize(sf::Vector2f(mCrosshair.getSize().x, mCrosshair.getSize().y));
    mCrosshairSprite.setFillColor(sf::Color(255, 255, 255, 150));
}

void Hud::update() {
    if(hasWorld()) {
        // target update
        mTarget.clear();
        if(getWorld()->hasLocalPlayer()) {
            game::Player* local = getWorld()->getLocalPlayer();
            for(unsigned int i=0;i<getWorld()->state()->getPlayers().size();i++) {
                game::Player* target = getWorld()->state()->getPlayers().at(i);
                if(target != local) {
                    s3dc::Sphere colTarget;
                    s3dc::Ray colRay;

                    colTarget.mPosition = target->transform()->getPosition();
                    colTarget.mRadius = target->transform()->getScale().x / 2;

                    colRay.mPosition = local->transform()->getPosition();
                    colRay.mDirection = local->transform()->forward(false);
                    colRay.mLength = 5;
                    if(s3dc::sphereRay(colTarget, colRay)) {
                        // display the targets name
                        mTarget = target->getName();
                    }
                }
            }
            // health update
            int newHealth = getWorld()->getLocalPlayer()->getHealth();
            if(newHealth != mLastHealth) {
                mLastHealth = newHealth;
                mHealth = Console::Variable::convertToString(newHealth);
            }
        }
    }
}
void Hud::render(sf::RenderTarget* target) {
    sf::Text text;
    if(hasTargetFont()) {
        if(!mTarget.empty()) {
            text.setFont(*getTargetFont());
            text.setColor(sf::Color(255, 255, 255, 150));
            text.setCharacterSize(40);

            text.setString(mTarget);
            text.setPosition(sf::Vector2f((int)(target->getSize().x / 2 - text.getLocalBounds().width / 2),
                                          (int)(target->getSize().y - text.getLocalBounds().width)));

            target->draw(text);
        }
    }
    if(hasHealthFont()) {
        if(!mHealth.empty()) {
            text.setFont(*getHealthFont());
            text.setColor(sf::Color(255, 255, 255, 200));
            text.setCharacterSize(30);

            text.setString(mHealth);
            text.setPosition(sf::Vector2f((int)(target->getSize().x - text.getLocalBounds().width - 5),
                                          (int)(target->getSize().y - text.getLocalBounds().height - 10)));

            target->draw(text);
        }
    }
    if(hasNotificationFont()) {
        if(!mNotification.empty() && mNotificationClock.getElapsedTime().asSeconds() < mNotificationTime) {
            text.setFont(*getNotificationFont());
            text.setColor(sf::Color(255, 255, 255, 200));
            text.setCharacterSize(30);

            text.setString(mNotification);
            text.setPosition(sf::Vector2f((int)(target->getSize().x / 2 - text.getLocalBounds().width / 2),
                                          (int)(target->getSize().y / 4 - text.getLocalBounds().height)));

            target->draw(text);
        }
    }
    mCrosshairSprite.setPosition((int)(target->getSize().x / 2 - mCrosshair.getSize().x / 2),
                                 (int)(target->getSize().y / 2 - mCrosshair.getSize().y / 2));
    target->draw(mCrosshairSprite);
}

void Hud::setTargetFont(sf::Font* font) {
    mTargetFont = font;
}
sf::Font* Hud::getTargetFont() {
    return mTargetFont;
}
const bool Hud::hasTargetFont() {
    return (mTargetFont != nullptr);
}

void Hud::setHealthFont(sf::Font* font) {
    mHealthFont = font;
}
sf::Font* Hud::getHealthFont() {
    return mHealthFont;
}
const bool Hud::hasHealthFont() {
    return (mHealthFont != nullptr);
}

void Hud::setNotificationFont(sf::Font* font) {
    mNotificationFont = font;
}
sf::Font* Hud::getNotificationFont() {
    return mNotificationFont;
}
const bool Hud::hasNotificationFont() {
    return (mNotificationFont != nullptr);
}

void Hud::setWorld(World* world) {
    mWorld = world;
}
World* Hud::getWorld() {
    return mWorld;
}
const bool Hud::hasWorld() {
    return (getWorld() != nullptr);
}
void Hud::setNotification(const std::string& notification, const float time) {
    mNotification = notification;
    mNotificationTime = time;
    mNotificationClock.restart();
}

#ifndef AOB_WORLD_H
#define AOB_WORLD_H

#include "s3d/scene.h"
#include "game/state.h"
#include "scoreboard.h"
#include "hud.h"

#include <SFML/Audio.hpp>

class World {
public:
    World();
    ~World();

    // updates the player models
    void update(const float dt);
    void render();

    void reset();
    void addLocalPlayers();
    void sync(game::State& newState);
    game::State* state();

    void setLocalPlayerName(const std::string& name);
    game::Player* getLocalPlayer();
    const bool hasLocalPlayer();

    void setScene(s3d::Scene* scene);
    void setPlayerGeometry(s3d::Geometry* geometry);
    void setPlayerMaterial(s3d::Material* material);

    s3d::Scene* getScene();
    s3d::Geometry* getPlayerGeometry();
    s3d::Material* getPlayerMaterial();

    const bool hasScene();
    const bool hasPlayerGeometry();
    const bool hasPlayerMaterial();

    Scoreboard* score();
    Hud* hud();
private:
    void updateConnections(game::State& newState);

    void updateModels();
    void deleteModels();

    void updateSounds();
    void updateDisplay();

    // local game state
    game::State mState;
    std::string mLocalPlayerName;

    // state rendering objects
    s3d::Scene* mScene;
    s3d::Geometry* mPlayerGeometry;
    s3d::Material* mPlayerMaterial;
    std::vector<s3d::Model*> mPlayerModels;

    // sound effects
    sf::SoundBuffer mJump;
    sf::Sound mJumpPlayer;

    sf::SoundBuffer mDeath;
    sf::Sound mDeathPlayer;

    sf::SoundBuffer mRespawn;
    sf::Sound mRespawnPlayer;

    sf::SoundBuffer mHurt;
    sf::Sound mHurtPlayer;

    sf::SoundBuffer mRaygun;
    sf::Sound mRaygunPlayer;

    // hud
    Scoreboard mScoreboard;
    Hud mHud;
};

#endif // AOB_WORLD_H

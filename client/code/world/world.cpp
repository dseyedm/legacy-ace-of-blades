#include "world.h"

World::World() {
    setScene(nullptr);
    setPlayerGeometry(nullptr);
    setPlayerMaterial(nullptr);

    mJump.loadFromFile("data/sound/jump.wav");
    mJumpPlayer.setBuffer(mJump);
    mDeath.loadFromFile("data/sound/death.wav");
    mDeathPlayer.setBuffer(mDeath);
    mRespawn.loadFromFile("data/sound/respawn.wav");
    mRespawnPlayer.setBuffer(mRespawn);
    mHurt.loadFromFile("data/sound/hurt.wav");
    mHurtPlayer.setBuffer(mHurt);
    mRaygun.loadFromFile("data/sound/ray.wav");
    mRaygunPlayer.setBuffer(mRaygun);

    score()->setWorld(this);
    hud()->setWorld(this);

    addLocalPlayers();
}
World::~World() {
    deleteModels();
}

void World::update(const float dt) {
    // graphics
    updateModels();
    // sound
    updateSounds();
    // hud
    updateDisplay();
}
void World::render() {
    if(hasScene()) {
        for(unsigned int i=0;i<mPlayerModels.size();i++) {
            if(state()->getPlayers().size() == mPlayerModels.size()) {
                s3d::Shader* shader = mPlayerModels.at(i)->material()->shader();
                shader->bind();
                shader->setUniform("player_team", state()->getPlayer(i)->getTeam());
                shader->unbind();
            }
            getScene()->renderModel(mPlayerModels.at(i));
        }
    }
}

void World::reset() {
    state()->removeAllPlayers();
    deleteModels();
}
void World::addLocalPlayers() {
    state()->addPlayer("Bob");
    const std::string localName = "You";
    state()->addPlayer(localName);
    setLocalPlayerName(localName);
}
void World::sync(game::State& newState) {
    updateConnections(newState);
    state()->set(newState);
}

game::State* World::state() {
    return &mState;
}

void World::setLocalPlayerName(const std::string& name) {
    mLocalPlayerName = name;
}
game::Player* World::getLocalPlayer() {
    return state()->getPlayer(mLocalPlayerName);
}
const bool World::hasLocalPlayer() {
    return (getLocalPlayer() != nullptr);
}

void World::setScene(s3d::Scene* scene) {
    mScene = scene;

    // add all of the models to the scene
    if(hasScene()) {
        updateModels();
    }
}
void World::setPlayerGeometry(s3d::Geometry* geometry) {
    mPlayerGeometry = geometry;

    if(hasPlayerGeometry()) {
        for(unsigned int i=0;i<mPlayerModels.size();i++) {
            mPlayerModels.at(i)->setGeometry(getPlayerGeometry());
        }
    }
}
void World::setPlayerMaterial(s3d::Material* material) {
    mPlayerMaterial = material;

    if(hasPlayerMaterial()) {
        for(unsigned int i=0;i<mPlayerModels.size();i++) {
            mPlayerModels.at(i)->setMaterial(getPlayerMaterial());
        }
    }
}
s3d::Scene* World::getScene() {
    return mScene;
}
s3d::Geometry* World::getPlayerGeometry() {
    return mPlayerGeometry;
}
s3d::Material* World::getPlayerMaterial() {
    return mPlayerMaterial;
}

const bool World::hasScene() {
    return (getScene() != nullptr);
}
const bool World::hasPlayerGeometry() {
    return (getPlayerGeometry() != nullptr);
}
const bool World::hasPlayerMaterial() {
    return (getPlayerMaterial() != nullptr);
}

Scoreboard* World::score() {
    return &mScoreboard;
}
Hud* World::hud() {
    return &mHud;
}

void World::updateConnections(game::State& newState) {
    if(state()->getPlayers().size() > 0 && newState.getPlayers().size() > 0) {
        if(state()->getPlayers().size() != newState.getPlayers().size()) {
            std::string notification;
            if(state()->getPlayers().size() < newState.getPlayers().size()) {
                // new player has joined
                game::Player* newPlayer = newState.getPlayers().back();
                notification = newPlayer->getName() + " joined";
            } else {
                // player has disconnected
                game::Player* disconnectedPlayer = nullptr;
                bool found = false;
                for(unsigned int i=0;i<newState.getPlayers().size();i++) {
                    if(state()->getPlayers().at(i)->getName() != newState.getPlayers().at(i)->getName()) {
                        found = true;
                        disconnectedPlayer = state()->getPlayers().at(i);
                    }
                }
                if(!found) {
                    disconnectedPlayer = state()->getPlayers().back();
                }
                notification = disconnectedPlayer->getName() + " left";
            }
            hud()->setNotification(notification);
        }
    }
}

void World::updateModels() {
    if(state()->getPlayers().size() != mPlayerModels.size()) {
        // delete old models and init new ones
        deleteModels();
        for(unsigned int i=0;i<state()->getPlayers().size();i++) {
            mPlayerModels.push_back(new s3d::Model);
            if(hasPlayerGeometry()) {
                mPlayerModels.back()->setGeometry(getPlayerGeometry());
            }
            if(hasPlayerMaterial()) {
                mPlayerModels.back()->setMaterial(getPlayerMaterial());
            }
        }
    }
    // update transformations
    for(unsigned int i=0;i<mPlayerModels.size();i++) {
        mPlayerModels.at(i)->transform()->setPosition   (state()->getPlayers().at(i)->transform()->getPosition());
        mPlayerModels.at(i)->transform()->setScale      (state()->getPlayers().at(i)->transform()->getScale());
        mPlayerModels.at(i)->transform()->setOrientation(state()->getPlayers().at(i)->transform()->getOrientation());
    }
}
void World::deleteModels() {
    for(unsigned int i=0;i<mPlayerModels.size();i++) {
        if(hasScene()) {
            getScene()->removeModel(mPlayerModels.at(i));
        }
        delete mPlayerModels.at(i);
    }
    mPlayerModels.clear();
}
void World::updateSounds() {
    game::Player* local = nullptr;
    if(hasLocalPlayer()) {
        local = getLocalPlayer();
        const glm::vec3 lPos = local->transform()->getPosition();
        const glm::vec3 lRot = local->transform()->forward(false);
        sf::Listener::setPosition(lPos.x, lPos.y, lPos.z);
        sf::Listener::setDirection(lRot.x, lRot.y, lRot.z);
    }
    for(unsigned int i=0;i<state()->getPlayers().size();i++) {
        game::Player* player = state()->getPlayers().at(i);
        glm::vec3 sourcePosition(player->transform()->getPosition());
        bool isLocal = false;
        if(hasLocalPlayer()) {
            isLocal = (player->getName() == local->getName());
            if(isLocal) {
                sourcePosition = glm::vec3(0);
            }
        }
        if(player->events()->mJumped) {
            mJumpPlayer.setRelativeToListener(isLocal);
            mJumpPlayer.setPosition(sourcePosition.x, sourcePosition.y, sourcePosition.z);
            mJumpPlayer.setVolume(50);
            mJumpPlayer.setAttenuation(0.6);
            if(mJumpPlayer.getStatus() != sf::Sound::Status::Playing) {
                mJumpPlayer.play();
            }
        }
        if(player->events()->mRespawned) {
            mRespawnPlayer.setRelativeToListener(isLocal);
            mRespawnPlayer.setPosition(sourcePosition.x, sourcePosition.y, sourcePosition.z);
            mRespawnPlayer.setVolume(70);
            mRespawnPlayer.setAttenuation(0.3);
            if(mRespawnPlayer.getStatus() != sf::Sound::Status::Playing) {
                mRespawnPlayer.play();
            }
        } else if(player->events()->mDied) {
            mDeathPlayer.setRelativeToListener(isLocal);
            mDeathPlayer.setPosition(sourcePosition.x, sourcePosition.y, sourcePosition.z);
            mDeathPlayer.setVolume(25);
            mDeathPlayer.setAttenuation(0.3);
            if(mDeathPlayer.getStatus() != sf::Sound::Status::Playing) {
                mDeathPlayer.play();
            }
        } else if(player->events()->mHurt) {
            mHurtPlayer.setRelativeToListener(isLocal);
            mHurtPlayer.setPosition(sourcePosition.x, sourcePosition.y, sourcePosition.z);
            mHurtPlayer.setVolume(70);
            mHurtPlayer.setAttenuation(0.3);
            if(mHurtPlayer.getStatus() != sf::Sound::Status::Playing) {
                mHurtPlayer.play();
            }
        }
        if(player->events()->mFired) {
            mRaygunPlayer.setRelativeToListener(isLocal);
            mRaygunPlayer.setPosition(sourcePosition.x, sourcePosition.y, sourcePosition.z);
            mRaygunPlayer.setVolume(65);
            mRaygunPlayer.setAttenuation(0.4);
            if(mRaygunPlayer.getStatus() != sf::Sound::Status::Playing) {
                mRaygunPlayer.play();
            }
        }
    }
}
void World::updateDisplay() {
    std::string local;
    if(hasLocalPlayer()) {
        local = getLocalPlayer()->getName();
    }
    // update kill / death notification
    for(unsigned int i=0;i<state()->getPlayers().size();i++) {
        game::Player* player = state()->getPlayers().at(i);
        if(player->events()->mDied) {
            std::string notification;
            const std::string predator = player->events()->mAssailant;
            const std::string prey = player->getName();
            if(predator == prey || predator.empty()) {
                // suicide
                if(prey == local) {
                    notification += "You";
                } else {
                    notification += prey;
                }
                notification += " suicided";
            } else {
                if(prey == local) {
                    notification += predator + " killed you";
                } else if(predator == local) {
                    notification += "You killed " + prey;
                } else {
                    notification += predator + " killed " + prey;
                }
            }
            hud()->setNotification(notification);
        }
    }
}























#ifndef AOB_CLIENT_H
#define AOB_CLIENT_H

#include <SFML/Network.hpp>
#include <iostream>

class Client {
public:
    Client();
    const bool bindToPort(const unsigned short port);
    void unbind();
    const unsigned short getPort();

    void update();

    void setTickrate(const float tickrate);
    void setTimeout(const float timeout);

    const float getTickrate() const;
    const float getTimeout() const;

    void setServerAddress(const sf::IpAddress& ipAddress);
    void setServerPort(const unsigned short port);

    const sf::IpAddress& getServerAddress() const;
    const unsigned short getServerPort() const;

protected:
    virtual void sendPacket() = 0;
    virtual void handlePacket(sf::Packet& serverPacket) = 0;
    virtual void timeout() = 0;

    sf::UdpSocket* socket();

private:
    sf::UdpSocket mSocket;
    sf::IpAddress mServerAddress;
    unsigned short mServerPort;

    sf::Clock mTickrateTimer;
    float mTickrate;
    sf::Clock mTimeoutTimer;
    float mTimeout;
};

#endif // AOB_CLIENT_H

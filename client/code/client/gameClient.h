#ifndef AOB_GAMECLIENT_H
#define AOB_GAMECLIENT_H

#include "client.h"
#include "network/packet.h"
#include "../world/world.h"

#include <console/console.h>

class GameClient : public Client {
public:
    GameClient();

    void connect(net::ClientPacket::Connect data = net::ClientPacket::Connect());
    void disconnect(net::ClientPacket::Disconnect data = net::ClientPacket::Disconnect());
    const bool connected();

    void send(net::ClientPacket& clPacket);

    void setConsole(Console* console);
    Console* getConsole();
    const bool hasConsole();

    void setWorld(World* world);
    World* getWorld();
    const bool hasWorld();
private:
    // protected
    void sendPacket();
    void handlePacket(sf::Packet& serverPacket);
    void timeout();

    // other
    void commandResponse(const std::string& response);

    bool mConnected;
    Console* mConsole;
    World* mGameWorld;
};

#endif // AOB_GAMECLIENT_H
